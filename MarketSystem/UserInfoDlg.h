#pragma once
#include "afxcmn.h"
#include "UserSet.h"
#include "RoleSet.h"

// CUserInfoDlg 窗体视图

class CUserInfoDlg : public CFormView
{
	DECLARE_DYNCREATE(CUserInfoDlg)

protected:
	CUserInfoDlg();           // 动态创建所使用的受保护的构造函数
	virtual ~CUserInfoDlg();

public:
	enum { IDD = USER_INFO };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
private:
	CListCtrl m_list;
	CUserSet* userSet;
	CRoleSet* roleSet;
public:
//	afx_msg void OnPaint();
};


