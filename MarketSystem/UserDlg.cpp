// UserDlg.cpp : 实现文件
//个人信息功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "MarketSystemDoc.h"
#include "UserDlg.h"
#include "RoleSet.h"
#include "UserSet.h"


// CUserDlg

IMPLEMENT_DYNCREATE(CUserDlg, CFormView)

CUserDlg::CUserDlg()
	: CFormView(CUserDlg::IDD)
	, m_user(_T(""))
	, m_name(_T(""))
	, m_newPwd(_T(""))
	, m_surePwd(_T(""))
	, m_oldPwd(_T(""))
{

}

CUserDlg::~CUserDlg()
{
}

void CUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_user);
	DDX_Text(pDX, IDC_EDIT2, m_name);
	DDX_Text(pDX, IDC_EDIT3, m_newPwd);
	DDX_Text(pDX, IDC_EDIT4, m_surePwd);
	DDX_Text(pDX, IDC_EDIT6, m_oldPwd);
}

BEGIN_MESSAGE_MAP(CUserDlg, CFormView)
	ON_BN_CLICKED(IDC_BUTTON3, &CUserDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CUserDlg::OnBnClickedButton4)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CUserDlg 诊断

#ifdef _DEBUG
void CUserDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUserDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CUserDlg 消息处理程序


void CUserDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();


	// TODO:  在此添加专用代码和/或调用基类
	long userId;
	
	//获取文档
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	//连接数据库
	userSet = doc->GetUserSet();
	roleSet = doc->GetRoleSet();


	//打开
	if (roleSet->IsOpen() == FALSE)
	{
		roleSet->Open();
	}
	if (userSet->IsOpen() == FALSE)
	{
		userSet->Open();
	}

	//重新查询 更新数据
	userSet->Requery();
	roleSet->Requery();

	//查询登录的用户id
	while (!userSet->IsEOF())
	{
		if (afx_userId == userSet->m_id)
		{
			userId = userSet->m_userRole;		//保存用户角色id(不能在此为全局变量userId赋值，因为SelectView在此之前加载)
			m_name = userSet->m_userName;		//为用户名控件变量赋值
			break;
		}
		userSet->MoveNext();		//依次遍历
	}

	////查询用户角色
	while (!roleSet->IsEOF())
	{
		//根据用户角色id查询角色名
		if (userId == roleSet->m_id)
		{
			m_user = roleSet->m_roleName;		
			break;
		}
		roleSet->MoveNext();		//依次遍历
	}

	UpdateData(false);		//将值更新到控件上
}


//确定按钮
void CUserDlg::OnBnClickedButton3()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);		//拿到控件的最新值
	if (m_oldPwd.IsEmpty() || m_newPwd.IsEmpty() || m_surePwd.IsEmpty())
	{
		MessageBox(TEXT("密码不能为空!"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	if (m_oldPwd != userSet->m_userPassword)
	{
		MessageBox(TEXT("旧密码错误!"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	if (m_newPwd == userSet->m_userPassword)
	{
		MessageBox(TEXT("新密码不能与旧密码相同!"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	if (m_newPwd != m_surePwd)
	{
		MessageBox(TEXT("两次输入的密码不一致！"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	//修改密码
	userSet->Edit();
	userSet->m_userPassword = m_newPwd;
	userSet->Update();
	MessageBox(TEXT("密码修改成功！"), TEXT("Success"), MB_ICONASTERISK);
	//清空控件值
	m_newPwd.Empty();
	m_surePwd.Empty();
	m_oldPwd.Empty();
	UpdateData(FALSE);		//更新
}

//取消按钮
void CUserDlg::OnBnClickedButton4()
{
	// TODO:  在此添加控件通知处理程序代码
	m_newPwd.Empty();
	m_surePwd.Empty();
	m_oldPwd.Empty();
	UpdateData(FALSE);		//更新
}

