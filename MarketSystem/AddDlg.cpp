// AddDlg.cpp : 实现文件
//添加商品功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "AddDlg.h"
#include "MarketSystemDoc.h"

// CAddDlg

IMPLEMENT_DYNCREATE(CAddDlg, CFormView)

CAddDlg::CAddDlg()
	: CFormView(CAddDlg::IDD)
	, m_count(0)
	, m_num1(0)
	, m_name2(_T(""))
	, m_price2(0)
	, m_num2(0)
{

}

CAddDlg::~CAddDlg()
{
}

void CAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	//  DDX_Text(pDX, IDC_EDIT1, m_price1);
	DDX_Text(pDX, IDC_EDIT2, m_num1);
	DDX_Text(pDX, IDC_EDIT7, m_name2);
	DDX_Text(pDX, IDC_EDIT5, m_price2);
	DDX_Text(pDX, IDC_EDIT6, m_num2);
	DDX_Text(pDX, IDC_EDIT1, m_count);
	//  DDX_Control(pDX, IDC_EDIT5, m_price2Ctrl);
}

BEGIN_MESSAGE_MAP(CAddDlg, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CAddDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CAddDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CAddDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON5, &CAddDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CAddDlg::OnBnClickedButton6)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CAddDlg 诊断

#ifdef _DEBUG
void CAddDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAddDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAddDlg 消息处理程序


void CAddDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	goodsSet = doc->GetGoodsSet();

	//打开数据集
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//重新查询 切换页面后数据集指针回到第一个数据
	//记录集是一个快照，必须调用Requery来反映添加和删除
	goodsSet->Requery();

	while (!goodsSet->IsEOF())
	{
		m_combo.AddString(CString(goodsSet->m_gName));	//将数据库中商品名称添加到视图的组合框中
		goodsSet->MoveNext();							//依次遍历
	}

	m_combo.SetCurSel(0);

	//初始化时调用事件进行赋值
	OnCbnSelchangeCombo1();
}

//组合框控件信息更改事件
void CAddDlg::OnCbnSelchangeCombo1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString name;
	int index = m_combo.GetCurSel();		//获取当前索引
	m_combo.GetLBText(index, name);			//将索引变量值赋给name变量
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//遍历数据集 获取选中商品库存与单价信息
	goodsSet->Requery();
	while (!goodsSet->IsEOF())
	{
		if (name == goodsSet->m_gName)
		{
			m_count = goodsSet->m_gCount;	//库存
			break;
		}
		goodsSet->MoveNext();
	}
	UpdateData(FALSE);				//刷新视图中的所有变量
}

//添加库存按钮功能实现
void CAddDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if (m_num1 <= 0)
	{
		MessageBox(TEXT("添加个数必须大于0!"), TEXT("添加失败"), MB_ICONERROR);
		return;
	}
	//添加到数据库
	goodsSet->Edit();
	goodsSet->m_gCount += m_num1;
	goodsSet->Update();

	MessageBox(TEXT("添加成功！"), TEXT("Success"), MB_ICONASTERISK);
	m_count += m_num1;			//更新视图中库存变量
	m_num1 = 0;
	UpdateData(FALSE);
}

//添加库存重置按钮功能实现
void CAddDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);			//此处获取变量m_name2 m_price2 m_num2的值以免将其重置
	m_num1 = 0;
	m_combo.SetCurSel(0);		//默认选择组合框第一项
	UpdateData(FALSE);
	OnCbnSelchangeCombo1();		//更新控件内容
}

//添加新商品功能实现
void CAddDlg::OnBnClickedButton5()
{
	// TODO:  在此添加控件通知处理程序代码
	bool flag = false;
	UpdateData(TRUE);

	if (m_name2.IsEmpty() || m_price2 < 0 || m_num2 <= 0)
	{
		MessageBox(TEXT("输入信息有误！"), TEXT("添加失败"), MB_ICONERROR);
		return;
	}

	//如果库存中已有该商品则只添加该商品的库存
	goodsSet->MoveFirst();
	while (!goodsSet->IsEOF())
	{
		if (m_name2 == goodsSet->m_gName)			//如果库存中已有该商品
		{
			flag = true;							
			goodsSet->Edit();
			goodsSet->m_gCount += m_num2;			//更新数据库数据信息
			goodsSet->Update();
			m_count += m_num2;						//更新视图中库存变量
			MessageBox(TEXT("已有该商品，添加库存成功！"), TEXT("Success"), MB_ICONASTERISK);
			break;
		}
		goodsSet->MoveNext();
	}

	//添加新商品
	if (!flag)
	{
		goodsSet->AddNew();
		goodsSet->m_gName = m_name2;
		goodsSet->m_gPrice = m_price2;
		goodsSet->m_gCount = m_num2;
		goodsSet->Update();
		//将新添加数据更新到组合框中
		m_combo.InsertString(0, m_name2);
		m_combo.SetCurSel(0);
		OnCbnSelchangeCombo1();
		MessageBox(TEXT("添加新商品成功！"), TEXT("Success"), MB_ICONASTERISK);
	}


	//重置控件信息
	m_name2.Empty();
	m_num2 = 0;
	m_price2 = 0;
	UpdateData(FALSE);
}

//添加新商品重置按钮功能实现
void CAddDlg::OnBnClickedButton6()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);		//此处获取变量m_num1的值以免将其重置为默认值
	m_name2.Empty();
	m_num2 = 0;
	m_price2 = 0;
	UpdateData(FALSE);
}

