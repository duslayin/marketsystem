#pragma once

#include "UserSet.h"
// CLoginDlg 对话框

class CLoginDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CLoginDlg();

// 对话框数据
	enum { IDD = DIALOG_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CString m_user;
	CString m_pwd;
	CUserSet* pUserSet;
public:
	afx_msg void OnBnClickedButton1();
	virtual void OnOK();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnClose();
	virtual void OnCancel();
//	afx_msg void OnPaint();
};

#ifndef _DEBUG  
inline CMarketSystemDoc* CMarketSystemView::GetDocument() const
{
	return reinterpret_cast<CMarketSystemDoc*>(m_pDocument);
}
#endif