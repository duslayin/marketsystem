#pragma once
#include "afxwin.h"
#include "UserSet.h"
#include "RoleSet.h"


// CUserDelDlg 窗体视图

class CUserDelDlg : public CFormView
{
	DECLARE_DYNCREATE(CUserDelDlg)

protected:
	CUserDelDlg();           // 动态创建所使用的受保护的构造函数
	virtual ~CUserDelDlg();

public:
	enum { IDD = USER_DEL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CString m_userRole;
	CComboBox m_combo;
	CUserSet* userSet;
	CRoleSet* roleSet;
public:
	virtual void OnInitialUpdate();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedButton1();
//	afx_msg void OnPaint();
};


