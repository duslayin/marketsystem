
// MainFrm.h : CMainFrame 类的接口
//

#pragma once
#include "MarketSystemView.h"
#include "MarketSystemDoc.h"

//WM_USER 是用户自定义消息的一个起始值
//WM_USER+100是为了区分系统消息和用户消息，避免冲突
#define NM_A	(WM_USER + 100)			//个人信息
#define NM_B	(WM_USER + 101)			//销售管理
#define NM_C	(WM_USER + 102)			//库存信息
#define NM_D	(WM_USER + 103)			//库存添加
#define NM_E	(WM_USER + 104)			//商品删除
#define NM_F	(WM_USER + 105)			//添加用户
#define NM_G	(WM_USER + 106)			//用户信息
#define NM_H	(WM_USER + 107)			//删除用户


class CMainFrame : public CFrameWnd
{
	
protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)
private:
	CSplitterWnd m_splitter;

// 特性
public:

// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CStatusBar        m_wndStatusBar;

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);

	afx_msg LRESULT OnMyChange(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void On32771();
	afx_msg void On32772();
	afx_msg void On32773();
	afx_msg void On32774();
	afx_msg void On32775();
	afx_msg void On32776();
};


