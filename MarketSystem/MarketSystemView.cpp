
// MarketSystemView.cpp : CMarketSystemView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MarketSystem.h"
#endif

#include "MarketSystemDoc.h"
#include "MarketSystemView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMarketSystemView

IMPLEMENT_DYNCREATE(CMarketSystemView, CFormView)

BEGIN_MESSAGE_MAP(CMarketSystemView, CFormView)
END_MESSAGE_MAP()

// CMarketSystemView 构造/析构

CMarketSystemView::CMarketSystemView()
	: CFormView(CMarketSystemView::IDD)
{
	// TODO:  在此处添加构造代码

}

CMarketSystemView::~CMarketSystemView()
{
}

void CMarketSystemView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CMarketSystemView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CFormView::PreCreateWindow(cs);
}

void CMarketSystemView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();


}


// CMarketSystemView 诊断

#ifdef _DEBUG
void CMarketSystemView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMarketSystemView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMarketSystemDoc* CMarketSystemView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMarketSystemDoc)));
	return (CMarketSystemDoc*)m_pDocument;
}
#endif //_DEBUG


// CMarketSystemView 消息处理程序
