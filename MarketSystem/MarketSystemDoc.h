
// MarketSystemDoc.h : CMarketSystemDoc 类的接口
//


#pragma once
#include "UserSet.h"
#include "RoleSet.h"
#include "GoodsSet.h"

class CMarketSystemDoc : public CDocument
{
protected: // 仅从序列化创建
	CMarketSystemDoc();
	DECLARE_DYNCREATE(CMarketSystemDoc)

private:
	CUserSet userSet;
	CRoleSet roleSet;
	CGoodsSet goodsSet;

// 特性
public:

// 操作
public:
	CUserSet* GetUserSet()
	{
		return &userSet;
	}

	CRoleSet* GetRoleSet()
	{
		return &roleSet;
	}
	
	CGoodsSet* GetGoodsSet()
	{
		return &goodsSet;
	}

// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 实现
public:
	virtual ~CMarketSystemDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 用于为搜索处理程序设置搜索内容的 Helper 函数
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
