// AddUserDlg.cpp : 实现文件
//添加用户功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "AddUserDlg.h"
#include "MarketSystemDoc.h"

// CAddUserDlg

IMPLEMENT_DYNCREATE(CAddUserDlg, CFormView)

CAddUserDlg::CAddUserDlg()
	: CFormView(CAddUserDlg::IDD)
	, m_userName(_T(""))
	, m_userPwd(_T(""))
	, m_userSurePwd(_T(""))
{

}

CAddUserDlg::~CAddUserDlg()
{
}

void CAddUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_userName);
	DDX_Text(pDX, IDC_EDIT2, m_userPwd);
	DDX_Text(pDX, IDC_EDIT3, m_userSurePwd);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
}

BEGIN_MESSAGE_MAP(CAddUserDlg, CFormView)
	ON_BN_CLICKED(IDC_BUTTON2, &CAddUserDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CAddUserDlg::OnBnClickedButton3)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CAddUserDlg 诊断

#ifdef _DEBUG
void CAddUserDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAddUserDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAddUserDlg 消息处理程序


void CAddUserDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	roleSet = doc->GetRoleSet();
	userSet = doc->GetUserSet();

	//打开数据集
	if (roleSet->IsOpen() == FALSE)
	{
		roleSet->Open();
	}

	if (userSet->IsOpen() == FALSE)
	{
		userSet->Open();
	}

	roleSet->Requery();

	while (!roleSet->IsEOF())
	{
		m_combo.AddString(CString(roleSet->m_roleName));	//将数据库中商品名称添加到视图的组合框中
		roleSet->MoveNext();							//依次遍历
	}
}

//添加按钮功能实现
void CAddUserDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if (m_userName.IsEmpty() || m_userPwd.IsEmpty() || m_userSurePwd.IsEmpty() || m_combo.GetCurSel() == CB_ERR)
	{
		MessageBox(TEXT("输入内容不能为空！"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	if (m_userPwd != m_userSurePwd)
	{
		MessageBox(TEXT("两次输入密码不一致！"), TEXT("错误"), MB_ICONERROR);
		return;
	}

	userSet->Requery();
	while (!userSet->IsEOF())
	{
		if (m_userName == userSet->m_userName)
		{
			MessageBox(TEXT("该用户已存在！"), TEXT("添加失败"), MB_ICONERROR);
			return;
		}
		userSet->MoveNext();
	}

	//添加用户
	userSet->AddNew();
	userSet->m_userName = m_userName;
	CString roleName;
	int index = m_combo.GetCurSel();
	m_combo.GetLBText(index, roleName);		//获取用户身份字符串
	roleSet->MoveFirst();
	while (!roleSet->IsEOF())
	{
		if (roleName == roleSet->m_roleName)	//找到该身份对应的id
		{
			userSet->m_userRole = roleSet->m_id;	//给新用户身份id赋值
			break;
		}
		roleSet->MoveNext();
	}
	userSet->m_userPassword = m_userPwd;
	userSet->Update();
	MessageBox(TEXT("添加新用户成功！"), TEXT("Success"), MB_ICONASTERISK);
	//重置编辑框
	m_userName.Empty();
	m_userPwd.Empty();
	m_userSurePwd.Empty();
	m_combo.SetCurSel(-1);
	UpdateData(FALSE);
}

//重置按钮功能实现
void CAddUserDlg::OnBnClickedButton3()
{
	// TODO:  在此添加控件通知处理程序代码
	m_userName.Empty();
	m_userPwd.Empty();
	m_userSurePwd.Empty();
	m_combo.SetCurSel(-1);
	UpdateData(FALSE);
}

