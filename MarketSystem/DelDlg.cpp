// DelDlg.cpp : 实现文件
//删除商品功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "DelDlg.h"
#include "MarketSystemDoc.h"

// CDelDlg

IMPLEMENT_DYNCREATE(CDelDlg, CFormView)

CDelDlg::CDelDlg()
	: CFormView(CDelDlg::IDD)
	, m_price(0)
	, m_count(0)
{

}

CDelDlg::~CDelDlg()
{
}

void CDelDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_EDIT2, m_price);
	DDX_Text(pDX, IDC_EDIT4, m_count);
}

BEGIN_MESSAGE_MAP(CDelDlg, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CDelDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CDelDlg::OnBnClickedButton1)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CDelDlg 诊断

#ifdef _DEBUG
void CDelDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDelDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDelDlg 消息处理程序


void CDelDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	goodsSet = doc->GetGoodsSet();

	//打开数据集
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//重新查询 切换页面后数据集指针回到第一个数据
	//记录集是一个快照，必须调用Requery来反映添加和删除
	goodsSet->Requery();

	while (!goodsSet->IsEOF())
	{
		m_combo.AddString(CString(goodsSet->m_gName));	//将数据库中商品名称添加到视图的组合框中
		goodsSet->MoveNext();							//依次遍历
	}

	m_combo.SetCurSel(0);

	//初始化时调用事件进行赋值
	OnCbnSelchangeCombo1();
}


void CDelDlg::OnCbnSelchangeCombo1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString name;
	int index = m_combo.GetCurSel();		//获取当前索引
	m_combo.GetLBText(index, name);			//将索引变量值赋给name变量
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//遍历数据集 获取选中商品库存与单价信息
	goodsSet->Requery();
	while (!goodsSet->IsEOF())
	{
		if (name == goodsSet->m_gName)
		{
			m_price = goodsSet->m_gPrice;	//单价
			m_count = goodsSet->m_gCount;	//库存
			break;
		}
		goodsSet->MoveNext();
	}
	UpdateData(FALSE);				//刷新视图中的所有变量
}

//删除按钮功能实现
void CDelDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	UINT i;
	i = MessageBox(_T("确定删除吗？"), _T("提示"), MB_YESNO | MB_ICONQUESTION);
	if (i == IDNO)
		return;
	int index = m_combo.GetCurSel();		//获取组合框中要删除的字符索引
	goodsSet->Delete();						//删除数据库中的数据
	m_combo.DeleteString(index);			//删除该商品在组合框中对应的字符
	m_combo.SetCurSel(0);					//删除后组合框回到第一个
	OnCbnSelchangeCombo1();					//更新视图数据
	MessageBox(TEXT("删除成功，商品已下架！"), TEXT("Success"), MB_ICONASTERISK);
}

