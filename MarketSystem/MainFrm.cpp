
// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "MarketSystem.h"

#include "MainFrm.h"
#include "SelectView.h"
#include "DisplayView.h"
#include "UserDlg.h"
#include "SellDlg.h"
#include "InfoDlg.h"
#include "AddDlg.h"
#include "DelDlg.h"
#include "AddUserDlg.h"
#include "UserInfoDlg.h"
#include "UserDelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	//ON_MESSAGE响应的是自定义消息
	//产生NM_X消息，自动调用OnMyChange函数
	ON_MESSAGE(NM_A, OnMyChange)
	ON_MESSAGE(NM_B, OnMyChange)
	ON_MESSAGE(NM_C, OnMyChange)
	ON_MESSAGE(NM_D, OnMyChange)
	ON_MESSAGE(NM_E, OnMyChange)
	ON_MESSAGE(NM_F, OnMyChange)
	ON_MESSAGE(NM_G, OnMyChange)
	ON_MESSAGE(NM_H, OnMyChange)
	ON_COMMAND(ID_32781, &CMainFrame::On32771)
	ON_COMMAND(ID_32772, &CMainFrame::On32772)
	ON_COMMAND(ID_32773, &CMainFrame::On32773)
	ON_COMMAND(ID_32774, &CMainFrame::On32774)
	ON_COMMAND(ID_32775, &CMainFrame::On32775)
	ON_COMMAND(ID_32776, &CMainFrame::On32776)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 状态行指示器
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO:  在此添加成员初始化代码
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("未能创建状态栏\n");
		return -1;      // 未能创建
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	SetClassLong(m_hWnd, GCL_HICON, (LONG)AfxGetApp()->LoadIconW(SYSTEM_ICON));		//设置图标
	SetTitle(TEXT("XXX"));		//设置主视图右侧标题
	MoveWindow(0, 0, 1000, 800);		//设置窗口大小
	CenterWindow();			//窗口居中显示
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return TRUE;
}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 消息处理程序



BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO:  在此添加专用代码和/或调用基类
	m_splitter.CreateStatic(this, 1, 2);		//创建1行2列静态窗口
	m_splitter.CreateView(0, 0, RUNTIME_CLASS(CSelectView), CSize(300, 800), pContext);
	m_splitter.CreateView(0, 1, RUNTIME_CLASS(CMarketSystemView), CSize(700, 800), pContext);
	//return CFrameWnd::OnCreateClient(lpcs, pContext);
	return TRUE;
}

LRESULT CMainFrame::OnMyChange(WPARAM wParam, LPARAM lParam)
{
	CCreateContext context;
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetActiveDocument();		//获取文档
	switch (wParam)				//使用switch-case结构时case后要加{}，否则会重定向，因为switch是一个代码块
	{
		//个人信息页面挂载
		case NM_A:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CUserDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档(不添加文档无法创建视图)
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CUserDlg), CSize(700, 800), &context);
			CUserDlg *pNewView = (CUserDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();		//初始化视图
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//销售管理页面挂载
		case NM_B:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CSellDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CSellDlg), CSize(700, 800), &context);
			CSellDlg *pNewView = (CSellDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//库存信息页面挂载
		case NM_C:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CInfoDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CInfoDlg), CSize(700, 800), &context);
			CInfoDlg *pNewView = (CInfoDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//库存添加页面挂载
		case NM_D:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CAddDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CAddDlg), CSize(700, 800), &context);
			CAddDlg *pNewView = (CAddDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//商品删除页面挂载
		case NM_E:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CDelDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CDelDlg), CSize(700, 800), &context);
			CDelDlg *pNewView = (CDelDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//添加用户页面挂载
		case NM_F:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CAddUserDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CAddUserDlg), CSize(700, 800), &context);
			CAddUserDlg *pNewView = (CAddUserDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//用户信息页面挂载
		case NM_G:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CUserInfoDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CUserInfoDlg), CSize(700, 800), &context);
			CUserInfoDlg *pNewView = (CUserInfoDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
			break;

		//删除用户页面挂载
		case NM_H:
		{
			context.m_pNewViewClass = RUNTIME_CLASS(CUserDelDlg);
			context.m_pCurrentDoc = doc;		//将在其上创建视图的文档 
			context.m_pCurrentFrame = this;
			context.m_pLastView = (CFormView*)m_splitter.GetPane(0, 1);
			m_splitter.DeleteView(0, 1);
			m_splitter.CreateView(0, 1, RUNTIME_CLASS(CUserDelDlg), CSize(700, 800), &context);
			CUserDelDlg *pNewView = (CUserDelDlg *)m_splitter.GetPane(0, 1);
			m_splitter.RecalcLayout();
			pNewView->OnInitialUpdate();
			m_splitter.SetActivePane(0, 1);
		}
	}

	return 0;
}

//退出菜单功能实现
void CMainFrame::On32771()
{
	// TODO:  在此添加命令处理程序代码
	exit(0);
}


//个人信息菜单功能实现
void CMainFrame::On32772()
{
	// TODO:  在此添加命令处理程序代码
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_A, (WPARAM)NM_A, (LPARAM)0);
}

//销售管理菜单功能实现
void CMainFrame::On32773()
{
	// TODO:  在此添加命令处理程序代码
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_B, (WPARAM)NM_B, (LPARAM)0);
}

//库存信息菜单功能实现
void CMainFrame::On32774()
{
	// TODO:  在此添加命令处理程序代码
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_C, (WPARAM)NM_C, (LPARAM)0);
}

//库存添加菜单功能实现
void CMainFrame::On32775()
{
	// TODO:  在此添加命令处理程序代码
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_D, (WPARAM)NM_D, (LPARAM)0);
}

//商品删除菜单功能实现
void CMainFrame::On32776()
{
	// TODO:  在此添加命令处理程序代码
	::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_E, (WPARAM)NM_E, (LPARAM)0);
}




