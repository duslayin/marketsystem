// UserInfoDlg.cpp : 实现文件
//用户信息功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "UserInfoDlg.h"
#include "MarketSystemDoc.h"

// CUserInfoDlg

IMPLEMENT_DYNCREATE(CUserInfoDlg, CFormView)

CUserInfoDlg::CUserInfoDlg()
	: CFormView(CUserInfoDlg::IDD)
{

}

CUserInfoDlg::~CUserInfoDlg()
{
}

void CUserInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
}

BEGIN_MESSAGE_MAP(CUserInfoDlg, CFormView)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CUserInfoDlg 诊断

#ifdef _DEBUG
void CUserInfoDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUserInfoDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CUserInfoDlg 消息处理程序


void CUserInfoDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	// 设置扩展风格
	//LVS_EX_FULLROWSELECT选中整行，LVS_EX_GRIDLINES网格
	m_list.SetExtendedStyle(m_list.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// 初始化表头
	CString field[] = { _T("ID"), _T("用户名"), _T("密码"), _T("权限") };
	for (int i = 0; i < sizeof(field) / sizeof(field[0]); ++i)
	{
		m_list.InsertColumn(i, field[i], LVCFMT_CENTER, 110);
	}

	//获取文档
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	//获取数据集
	userSet = doc->GetUserSet();
	roleSet = doc->GetRoleSet();
	//打开数据集
	if (userSet->IsOpen() == FALSE)
	{
		userSet->Open();
	}
	if (roleSet->IsOpen() == FALSE)
	{
		roleSet->Open();
	}
	//更新数据
	userSet->Requery();

	int i = 0;			//行号
	CString info;
	//遍历数据集取数据
	while (!userSet->IsEOF())
	{
		//确定行
		info.Format(TEXT("%d"), i + 1);						//行号
		m_list.InsertItem(i, info);							//用户id
		int j = 1;		//列号
		//列添加数据
		m_list.SetItemText(i, j++, userSet->m_userName);		//用户名
		m_list.SetItemText(i, j++, userSet->m_userPassword);	//密码
		roleSet->MoveFirst();
		while (!roleSet->IsEOF())								//根据身份ID查询身份字符串
		{
			if (roleSet->m_id == userSet->m_userRole)
			{
				info = roleSet->m_roleName;						//用户身份（权限）
				break;
			}
			roleSet->MoveNext();
		}
		m_list.SetItemText(i, j++, info);					
		i++;			//行号加1
		userSet->MoveNext();
	}
}

