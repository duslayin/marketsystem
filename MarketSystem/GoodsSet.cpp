// GoodsSet.h : CGoodsSet 类的实现



// CGoodsSet 实现

// 代码生成在 2022年5月5日, 15:32

#include "stdafx.h"
#include "GoodsSet.h"
IMPLEMENT_DYNAMIC(CGoodsSet, CRecordset)

CGoodsSet::CGoodsSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_id = 0;
	m_gName = L"";
	m_gCount = 0;
	m_gPrice = 0.0;
	m_nFields = 4;
	m_nDefaultType = snapshot;
}
//#error 安全问题：连接字符串可能包含密码。
// 此连接字符串中可能包含明文密码和/或其他重要
// 信息。请在查看完此连接字符串并找到所有与安全
// 有关的问题后移除 #error。可能需要将此密码存
// 储为其他格式或使用其他的用户身份验证。
CString CGoodsSet::GetDefaultConnect()
{
	return _T("Driver=MySQL ODBC 8.0 Unicode Driver;SERVER=127.0.0.1;UID=root;PWD=123456;DATABASE=market;PORT=3306");
}

CString CGoodsSet::GetDefaultSQL()
{
	return _T("[market_goods]");
}

void CGoodsSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 和 RFX_Int() 这类宏依赖的是
// 成员变量的类型，而不是数据库字段的类型。
// ODBC 尝试自动将列值转换为所请求的类型
	RFX_Long(pFX, _T("[id]"), m_id);
	RFX_Text(pFX, _T("[gName]"), m_gName);
	RFX_Long(pFX, _T("[gCount]"), m_gCount);
	RFX_Double(pFX, _T("[gPrice]"), m_gPrice);

}
/////////////////////////////////////////////////////////////////////////////
// CGoodsSet 诊断

#ifdef _DEBUG
void CGoodsSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CGoodsSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


