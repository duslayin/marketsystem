// SelectView.cpp : 实现文件
//选择页面功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "SelectView.h"
#include "MainFrm.h"

// CSelectView

IMPLEMENT_DYNCREATE(CSelectView, CTreeView)

CSelectView::CSelectView()
{

}

CSelectView::~CSelectView()
{
}

BEGIN_MESSAGE_MAP(CSelectView, CTreeView)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, &CSelectView::OnTvnSelchanged)
END_MESSAGE_MAP()


// CSelectView 诊断

#ifdef _DEBUG
void CSelectView::AssertValid() const
{
	CTreeView::AssertValid();
}

#ifndef _WIN32_WCE
void CSelectView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSelectView 消息处理程序


void CSelectView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	
	//添加图片
	HICON icon[8];
	icon[0] = AfxGetApp()->LoadIconW(LOGIN_USER);
	icon[1] = AfxGetApp()->LoadIconW(GOODS_SELL);
	icon[2] = AfxGetApp()->LoadIconW(GOODS_INFO);
	icon[3] = AfxGetApp()->LoadIconW(GOODS_ADD);
	icon[4] = AfxGetApp()->LoadIconW(GOODS_DEL);
	icon[5] = AfxGetApp()->LoadIconW(USER_ADD);
	icon[6] = AfxGetApp()->LoadIconW(USER_INFO);
	icon[7] = AfxGetApp()->LoadIconW(USER_DEL);

	//图片列表的创建 CImageList::Create
	//40, 40：指定图标的宽度和高度
	//ILC_COLOR32：图标格式
	//8, 8：有8个图标
	m_imageList.Create(40, 40, ILC_COLOR32, 8, 8);
	for (int i = 0; i < 8; i++)
	{
		m_imageList.Add(icon[i]);
	}

	//获取数视图中的树控件
	m_treeCtrl = &GetTreeCtrl();
	//数控件设置图片列表
	m_treeCtrl->SetImageList(&m_imageList, TVSIL_NORMAL);
	//树控件设置节点
	m_treeCtrl->InsertItem(TEXT("个人信息"), 0, 0, NULL);
	m_treeCtrl->InsertItem(TEXT("销售管理"), 1, 1, NULL);
	m_treeCtrl->InsertItem(TEXT("库存信息"), 2, 2, NULL);
	m_treeCtrl->InsertItem(TEXT("库存添加"), 3, 3, NULL);
	m_treeCtrl->InsertItem(TEXT("商品删除"), 4, 4, NULL);

	if (afx_userRole == 1)							//如果身份是管理员，则有权操作用户信息
	{
		m_treeCtrl->InsertItem(TEXT("添加用户"), 5, 5, NULL);
		m_treeCtrl->InsertItem(TEXT("用户信息"), 6, 6, NULL);
		m_treeCtrl->InsertItem(TEXT("删除用户"), 7, 7, NULL);
	}
}

//切换左侧视图树控件操作
void CSelectView::OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;

	HTREEITEM item;
	item = m_treeCtrl->GetSelectedItem();
	CString str;
	str = m_treeCtrl->GetItemText(item);
	//MessageBox(str);
	if (str == TEXT("个人信息"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_A, (WPARAM)NM_A, (LPARAM)0);
	}

	else if (str == TEXT("销售管理"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_B, (WPARAM)NM_B, (LPARAM)0);
	}
	else if (str == TEXT("库存信息"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_C, (WPARAM)NM_C, (LPARAM)0);
	}
	else if (str == TEXT("库存添加"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_D, (WPARAM)NM_D, (LPARAM)0);
	}
	else if (str == TEXT("商品删除"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_E, (WPARAM)NM_E, (LPARAM)0);
	}
	else if (str == TEXT("添加用户"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_F, (WPARAM)NM_F, (LPARAM)0);
	}
	else if (str == TEXT("用户信息"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_G, (WPARAM)NM_G, (LPARAM)0);
	}
	else if (str == TEXT("删除用户"))
	{
		::PostMessage(AfxGetMainWnd()->GetSafeHwnd(), NM_H, (WPARAM)NM_H, (LPARAM)0);
	}
}
