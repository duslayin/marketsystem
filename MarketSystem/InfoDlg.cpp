// InfoDlg.cpp : 实现文件
//库存信息视图功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "InfoDlg.h"
#include "MarketSystemDoc.h"


// CInfoDlg

IMPLEMENT_DYNCREATE(CInfoDlg, CFormView)

CInfoDlg::CInfoDlg()
	: CFormView(CInfoDlg::IDD)
{

}

CInfoDlg::~CInfoDlg()
{
}

void CInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
}

BEGIN_MESSAGE_MAP(CInfoDlg, CFormView)
	ON_BN_CLICKED(IDC_BUTTON1, &CInfoDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CInfoDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CInfoDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CInfoDlg::OnBnClickedButton4)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CInfoDlg 诊断

#ifdef _DEBUG
void CInfoDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CInfoDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CInfoDlg 消息处理程序


void CInfoDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	// 设置扩展风格
	//LVS_EX_FULLROWSELECT选中整行，LVS_EX_GRIDLINES网格
	m_list.SetExtendedStyle(m_list.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// 初始化表头
	CString field[] = { _T("ID"), _T("商品名称"), _T("商品价格"), _T("库存数量") };
	for (int i = 0; i < sizeof(field) / sizeof(field[0]); ++i)
	{
		m_list.InsertColumn(i, field[i], LVCFMT_CENTER, 120);
	}

	//获取文档
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	//获取数据集
	goodsSet = doc->GetGoodsSet();
	//打开数据集
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//更新数据
	goodsSet->Requery();

	int i = 0;			//行号
	CString info;
	//遍历数据集取数据
	while (!goodsSet->IsEOF())
	{
		//确定行
		info.Format(TEXT("%d"), i + 1);						//行号
		m_list.InsertItem(i, info);							//商品id
		int j = 1;		//列号
		//列添加数据
		m_list.SetItemText(i, j++, goodsSet->m_gName);		//商品名称
		info.Format(TEXT("%.3lf"), goodsSet->m_gPrice);		
		m_list.SetItemText(i, j++, info);					//商品价格
		info.Format(TEXT("%d"), goodsSet->m_gCount);
		m_list.SetItemText(i, j++, info);					//商品库存
		i++;			//行号加1
		goodsSet->MoveNext();
	}
}

//按价格升序按钮功能实现
void CInfoDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	goodsSet->m_strSort = TEXT("gPrice asc, gCount");		//按价格升序排列
	goodsSet->Requery();									//重新查询数据集
	int i = 0;												//行号
	CString info;
	while (!goodsSet->IsEOF())
	{
		int j = 1;											//从第二列开始修改	
		m_list.SetItemText(i, j++, goodsSet->m_gName);		//商品名称
		info.Format(TEXT("%.3lf"), goodsSet->m_gPrice);
		m_list.SetItemText(i, j++, info);					//商品价格
		info.Format(TEXT("%d"), goodsSet->m_gCount);
		m_list.SetItemText(i, j++, info);					//商品库存
		i++;			//行号加1
		goodsSet->MoveNext();
	}
}

//按价格降序按钮功能实现
void CInfoDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	goodsSet->m_strSort = TEXT("gPrice desc, gCount");		//按价格降序排列
	goodsSet->Requery();									//重新查询数据集
	int i = 0;												//行号
	CString info;
	while (!goodsSet->IsEOF())
	{
		int j = 1;											//从第二列开始修改	
		m_list.SetItemText(i, j++, goodsSet->m_gName);		//商品名称
		info.Format(TEXT("%.3lf"), goodsSet->m_gPrice);
		m_list.SetItemText(i, j++, info);					//商品价格
		info.Format(TEXT("%d"), goodsSet->m_gCount);
		m_list.SetItemText(i, j++, info);					//商品库存
		i++;			//行号加1
		goodsSet->MoveNext();
	}
}

//按库存升序按钮功能实现
void CInfoDlg::OnBnClickedButton3()
{
	// TODO:  在此添加控件通知处理程序代码
	goodsSet->m_strSort = TEXT("gCount asc, gPrice");		//按库存升序排列
	goodsSet->Requery();									//重新查询数据集
	int i = 0;												//行号
	CString info;
	while (!goodsSet->IsEOF())
	{
		int j = 1;											//从第二列开始修改	
		m_list.SetItemText(i, j++, goodsSet->m_gName);		//商品名称
		info.Format(TEXT("%.3lf"), goodsSet->m_gPrice);
		m_list.SetItemText(i, j++, info);					//商品价格
		info.Format(TEXT("%d"), goodsSet->m_gCount);
		m_list.SetItemText(i, j++, info);					//商品库存
		i++;			//行号加1
		goodsSet->MoveNext();
	}
}

//按库存降序按钮功能实现
void CInfoDlg::OnBnClickedButton4()
{
	// TODO:  在此添加控件通知处理程序代码
	goodsSet->m_strSort = TEXT("gCount desc, gPrice");		//按库存降序排列
	goodsSet->Requery();									//重新查询数据集
	int i = 0;												//行号
	CString info;
	while (!goodsSet->IsEOF())
	{
		int j = 1;											//从第二列开始修改	
		m_list.SetItemText(i, j++, goodsSet->m_gName);		//商品名称
		info.Format(TEXT("%.3lf"), goodsSet->m_gPrice);
		m_list.SetItemText(i, j++, info);					//商品价格
		info.Format(TEXT("%d"), goodsSet->m_gCount);
		m_list.SetItemText(i, j++, info);					//商品库存
		i++;			//行号加1
		goodsSet->MoveNext();
	}
}
