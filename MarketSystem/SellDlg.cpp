// SellDlg.cpp : 实现文件
//销售管理功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "SellDlg.h"
#include "MarketSystemDoc.h"

// CSellDlg

IMPLEMENT_DYNCREATE(CSellDlg, CFormView)

CSellDlg::CSellDlg()
	: CFormView(CSellDlg::IDD)
	, m_price(0)
	, m_left(0)
	, m_num(0)
	, m_sellInfo(_T(""))
{

}

CSellDlg::~CSellDlg()
{
}

void CSellDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Text(pDX, IDC_EDIT1, m_price);
	DDX_Text(pDX, IDC_EDIT2, m_left);
	DDX_Text(pDX, IDC_EDIT5, m_num);
	DDX_Text(pDX, IDC_EDIT6, m_sellInfo);
}

BEGIN_MESSAGE_MAP(CSellDlg, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CSellDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CSellDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSellDlg::OnBnClickedButton2)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CSellDlg 诊断

#ifdef _DEBUG
void CSellDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSellDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSellDlg 消息处理程序


void CSellDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	goodsSet = doc->GetGoodsSet();

	//打开数据集
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//重新查询 切换页面后数据集指针回到第一个数据
	//记录集是一个快照，必须调用Requery来反映添加和删除
	goodsSet->Requery();

	while (!goodsSet->IsEOF())
	{
		m_combo.AddString(CString(goodsSet->m_gName));	//将数据库中商品名称添加到视图的组合框中
		goodsSet->MoveNext();							//依次遍历
	}

	m_combo.SetCurSel(0);

	//初始化时调用事件进行赋值
	OnCbnSelchangeCombo1();
}

//组合框控件信息更改事件
void CSellDlg::OnCbnSelchangeCombo1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString name;
	int index = m_combo.GetCurSel();		//获取当前索引
	m_combo.GetLBText(index, name);			//将索引变量值赋给name变量
	if (goodsSet->IsOpen() == FALSE)
	{
		goodsSet->Open();
	}
	//遍历数据集 获取选中商品库存与单价信息
	goodsSet->Requery();
	while (!goodsSet->IsEOF())
	{
		if (name == goodsSet->m_gName)
		{
			m_price = goodsSet->m_gPrice;
			m_left = goodsSet->m_gCount;
			break;
		}
		goodsSet->MoveNext();
	}
	UpdateData(FALSE);
}

//购买按钮功能实现
void CSellDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	//因数据集指针停留在组合框所选商品数据位置，所以不用重新遍历数据集
	CString sellInfo;
	UpdateData(TRUE);		//获取控件变量值
	if (m_num <= 0)
	{
		MessageBox(TEXT("购买个数必须大于0!"), TEXT("购买失败"), MB_ICONERROR);
		m_num = 0;
		UpdateData(FALSE);
		return;
	}

	if (m_num > goodsSet->m_gCount)
	{
		MessageBox(TEXT("购买数量超过库存量!"), TEXT("购买失败"), MB_ICONERROR);
		m_num = 0;
		UpdateData(FALSE);
		return;
	}
	//更新数据库中库存数据
	goodsSet->Edit();
	goodsSet->m_gCount -= m_num;
	goodsSet->Update();
	m_left = goodsSet->m_gCount;
	sellInfo.Format(_T("商品：%s \r\n单价：%.3lf \r\n个数：%d \r\n总价：%.3lf"), goodsSet->m_gName, m_price, m_num, m_price * m_num);
	m_sellInfo = sellInfo;
	MessageBox(TEXT("购买成功！"), TEXT("Success"), MB_ICONASTERISK);

	m_num = 0;			//购买个数置0
	UpdateData(FALSE);	//更新控件数据
}

//重置按钮功能实现
void CSellDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	m_combo.SetCurSel(0);
	m_num = 0;
	m_sellInfo.Empty();
	UpdateData(FALSE);
}
