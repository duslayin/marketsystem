// LoginDlg.cpp : 实现文件
//登录对话框功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "LoginDlg.h"
#include "afxdialogex.h"
#include "MarketSystemDoc.h"
#include "MarketSystemView.h"


// CLoginDlg 对话框

IMPLEMENT_DYNAMIC(CLoginDlg, CDialogEx)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLoginDlg::IDD, pParent)
	, m_user(_T("张三"))
	, m_pwd(_T("12345"))
{
	
}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_user);
	DDX_Text(pDX, IDC_EDIT2, m_pwd);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CLoginDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CLoginDlg::OnBnClickedButton2)
	ON_WM_CLOSE()
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CLoginDlg 消息处理程序

//登录按钮功能实现
void CLoginDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString m_sql;
	CString user, pwd;
	//CDatabase db;
	//pUserSet = new CUserSet(&db);		//传入db即可
	pUserSet = new CUserSet(&((CMarketSystemApp*)AfxGetApp())->m_database);
	/*m_sql.Format(_T("select * from market_user"));
	pUserSet->Open(AFX_DB_USE_DEFAULT_TYPE, m_sql);*/
	pUserSet->Open();
	UpdateData(true);
	//判断编辑框是否为空
	if (m_user.IsEmpty() || m_pwd.IsEmpty())
	{
		MessageBox(TEXT("用户名或密码不能为空！"), TEXT("错误"), MB_ICONERROR);
		return;
	}
	pUserSet->MoveFirst();
	while (!pUserSet->IsEOF())
	{
		user = pUserSet->m_userName;
		pwd = pUserSet->m_userPassword;
		if (user == m_user && pwd == m_pwd)
		{
			afx_userId = pUserSet->m_id;					//将用户身份id保存在全局变量中
			afx_userRole = pUserSet->m_userRole;			//保存用户角色id到全局变量中
			CDialogEx::OnOK();
			return;
		}
		pUserSet->MoveNext();
	}
	MessageBox(TEXT("用户名或密码错误！"), TEXT("错误"), MB_ICONERROR);
	return;
}

//取消按钮功能实现
void CLoginDlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	exit(0);
}

//防止输入回车进入主页面
void CLoginDlg::OnOK()
{
	// TODO:  在此添加专用代码和/或调用基类

	//CDialogEx::OnOK();
}



//点击×图标直接退出
void CLoginDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	//CDialogEx::OnClose();
	exit(0);
}

//防止输入ESC键进入主页面
void CLoginDlg::OnCancel()
{
	// TODO:  在此添加专用代码和/或调用基类

	//CDialogEx::OnCancel();
}
