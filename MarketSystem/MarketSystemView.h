
// MarketSystemView.h : CMarketSystemView 类的接口
//

#pragma once

#include "resource.h"
#include "UserSet.h"
#include "RoleSet.h"
#include "MarketSystemDoc.h"

class CMarketSystemView : public CFormView
{
protected: // 仅从序列化创建
	CMarketSystemView();
	DECLARE_DYNCREATE(CMarketSystemView)

public:
	enum{ IDD = IDD_MARKETSYSTEM_FORM };

// 特性
public:
	CMarketSystemDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~CMarketSystemView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // MarketSystemView.cpp 中的调试版本
inline CMarketSystemDoc* CMarketSystemView::GetDocument() const
   { return reinterpret_cast<CMarketSystemDoc*>(m_pDocument); }
#endif

