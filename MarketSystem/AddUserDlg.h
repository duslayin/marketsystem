#pragma once
#include "afxwin.h"
#include "RoleSet.h"
#include "UserSet.h"

// CAddUserDlg 窗体视图

class CAddUserDlg : public CFormView
{
	DECLARE_DYNCREATE(CAddUserDlg)

protected:
	CAddUserDlg();           // 动态创建所使用的受保护的构造函数
	virtual ~CAddUserDlg();

public:
	enum { IDD = USER_ADD };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CString m_userName;
	CString m_userPwd;
	CString m_userSurePwd;
	CComboBox m_combo;
	CRoleSet* roleSet;
	CUserSet* userSet;
public:
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
//	afx_msg void OnPaint();
};


