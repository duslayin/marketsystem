// UserDelDlg.cpp : 实现文件
//删除用户功能实现

#include "stdafx.h"
#include "MarketSystem.h"
#include "UserDelDlg.h"
#include "MarketSystemDoc.h"

// CUserDelDlg

IMPLEMENT_DYNCREATE(CUserDelDlg, CFormView)

CUserDelDlg::CUserDelDlg()
	: CFormView(CUserDelDlg::IDD)
	, m_userRole(_T(""))
{

}

CUserDelDlg::~CUserDelDlg()
{
}

void CUserDelDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//  DDX_Text(pDX, IDC_EDIT1, m_userName);
	DDX_Text(pDX, IDC_EDIT2, m_userRole);
	DDX_Control(pDX, IDC_COMBO1, m_combo);
}

BEGIN_MESSAGE_MAP(CUserDelDlg, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CUserDelDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CUserDelDlg::OnBnClickedButton1)
//	ON_WM_PAINT()
END_MESSAGE_MAP()


// CUserDelDlg 诊断

#ifdef _DEBUG
void CUserDelDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUserDelDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CUserDelDlg 消息处理程序


void CUserDelDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO:  在此添加专用代码和/或调用基类
	CMarketSystemDoc* doc = (CMarketSystemDoc*)GetDocument();
	userSet = doc->GetUserSet();
	roleSet = doc->GetRoleSet();

	//打开数据集
	if (userSet->IsOpen() == FALSE)
	{
		userSet->Open();
	}
	if (roleSet->IsOpen() == FALSE)
	{
		roleSet->Open();
	}
	//重新查询 切换页面后数据集指针回到第一个数据
	//记录集是一个快照，必须调用Requery来反映添加和删除
	userSet->Requery();

	while (!userSet->IsEOF())
	{
		m_combo.AddString(CString(userSet->m_userName));	//将数据库中用户名添加到视图的组合框中
		userSet->MoveNext();							//依次遍历
	}

	m_combo.SetCurSel(0);

	//初始化时调用事件进行赋值
	OnCbnSelchangeCombo1();
}

//组合框内容切换事件
void CUserDelDlg::OnCbnSelchangeCombo1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString userName;
	int index = m_combo.GetCurSel();		//获取当前索引
	m_combo.GetLBText(index, userName);			//将索引变量值赋给name变量
	if (userSet->IsOpen() == FALSE)
	{
		userSet->Open();
	}
	if (roleSet->IsOpen() == FALSE)
	{
		roleSet->Open();
	}
	//遍历数据集 获取选中商品库存与单价信息
	userSet->Requery();
	while (!userSet->IsEOF())
	{
		if (userName == userSet->m_userName)
		{
			roleSet->MoveFirst();
			while (!roleSet->IsEOF())								//根据身份ID查询身份字符串
			{
				if (roleSet->m_id == userSet->m_userRole)
				{
					m_userRole = roleSet->m_roleName;						//用户身份（权限）
					break;
				}
				roleSet->MoveNext();
			}
			break;							//必须break否则无法获取当前组合框选中用户
		}
		userSet->MoveNext();
	}
	UpdateData(FALSE);				//刷新视图中的所有变量
}

//删除按钮功能实现
void CUserDelDlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	UINT i;
	i = MessageBox(_T("确定删除该用户吗？"), _T("提示"), MB_YESNO | MB_ICONQUESTION);
	if (i == IDNO)
		return;
	int index = m_combo.GetCurSel();		//获取组合框中要删除的字符索引
	userSet->Delete();						//删除数据库中的数据
	m_combo.DeleteString(index);			//删除该用户在组合框中对应的字符
	m_combo.SetCurSel(0);					//删除后组合框回到第一个
	OnCbnSelchangeCombo1();					//更新视图数据
	MessageBox(TEXT("删除成功，该用户信息已注销！"), TEXT("Success"), MB_ICONASTERISK);
}
