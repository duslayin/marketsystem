/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.36 : Database - market
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`market` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `market`;

/*Data for the table `market_goods` */

insert  into `market_goods`(`id`,`gName`,`gCount`,`gPrice`) values 
(1,'娃哈哈',45,2.00),
(2,'可口可乐',211,3.00),
(4,'卫龙大面筋',24,6.00),
(5,'农夫山泉',10,2.00),
(7,'红烧牛肉面',2,4.80),
(10,'雪碧',1,2.99),
(11,'RTX3090',8,99.99),
(13,'老冰棍',6,0.50),
(14,'八个核桃',1,6.80),
(15,'奥利给',1,999999.00),
(16,'iPhone22 Pro Max',5,8999.00),
(21,'老坛酸菜牛肉面',5,4.88);

/*Data for the table `market_role` */

insert  into `market_role`(`id`,`roleName`) values 
(1,'管理员'),
(2,'售货员');

/*Data for the table `market_user` */

insert  into `market_user`(`id`,`userName`,`userPassword`,`userRole`) values 
(1,'张三','12345',1),
(2,'李四','lisi666',2),
(3,'小强','qqq666',2),
(4,'张伟','zzz333',2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
